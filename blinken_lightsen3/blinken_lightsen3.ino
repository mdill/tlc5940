#include "Tlc5940.h"

/*
BSD 2-Clause License

Copyright (c) 2017, Michael Dill
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
    Michael Dill - 2017027

    This sketch is designed to exercize the TLC5940 as a proof of concept.  In
    order to control LEDs, individually, four photoresistors must be connected
    to the Arduino.

    The Arduino will take note of which photocells are "dark" at any given time,
    and set pre-determined LEDs on/off accordingly.

 */

// Set the "trigger" for the light level to turn LEDs on/off
const int trigger = 576;
// Set the delay/refresh rate for the sketch
const byte wait = 50;

// Set the four pins the photoresistors will be connected to
const byte group1 = 0;
const byte group2 = 1;
const byte group3 = 2;
const byte group4 = 3;

class LED{
    private:
        int brightness = 0;  // Set the initial brightness for the LEDs
        byte change = 15;  // Set the inc/decrement rate for the LEDs

    public:
        LED( byte group );
        int statusLevel = 0;  // Get the "brightness" seen by the photocell
        byte pin;
        void getStatus();
        void ledChange( byte lowest, byte highest );
};

// Define a constructor for our class
LED::LED( byte group ){
    pin = group;
}

// Define a method to change the brightness for our LED "group"
void LED::ledChange( byte lowest, byte highest ){
    // Check the first "group" with the first four LEDs
    if( statusLevel <= trigger ){
        for( byte pinNum = lowest; pinNum <= highest; pinNum++ ){
            Tlc.set( pinNum, brightness );
        }
        
        brightness += change;

        if( brightness >= 4095 || brightness <= 1 )
            change = -change;
    }
    Tlc.update();
}

// Define a method to get the information for each photoresistor
void LED::getStatus(){
    statusLevel = analogRead( pin );
}

    // Create new classes, for each "group" of LEDs
    LED w(group1), x(group2), y(group3), z(group4);

void setup(){
    Tlc.init( 0 );
}

void loop(){
    // Give ourselves a blank slate, to begin with
    if( w.statusLevel > trigger && x.statusLevel > trigger
            && y.statusLevel > trigger && z.statusLevel > trigger ){
        Tlc.clear();
    }

    // Update our LEDs, according to individual photocell values
    w.getStatus();
    x.getStatus();
    y.getStatus();
    z.getStatus();

    // Update our LED levels, if necessary
    w.ledChange( 0, 3 );
    x.ledChange( 4, 7 );
    y.ledChange( 8, 11 );
    z.ledChange( 12, 15 );

    Tlc.update();
    delay( wait );
}

