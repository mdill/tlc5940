# Arduino TLC5940 compilation

This directory is full of sketches for the TLC5940 on the Arduino platform.

These sketches are available for educational purposes only, and any damage
resulting from the use of any code found here is the sole responsibility of the
user.

Any sketches that invoke the use of GPIO or other hardware level items must be
run using the `sudo` command in order to access hardware level systems.
Without this command, the GPIO/hardware will not function properly.

Any sketches which do not call upon GPIO can/will function properly without the
use of root level access.

## Files

Each directory contains a different sketch, in order to implement the TLC5940
in a different way.  Instructions for each sketch can be found in the comment
section, at the beginning of each respective sketch.

## Downloading

    git clone https://bitbucket.org/mdill/TLC5940.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/tlc5940/src/ddc1b1663dac6adb57c8422208c2338ea9c6390f/LICENSE.txt?at=master) file for
details.

