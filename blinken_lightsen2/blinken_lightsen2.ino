#include "Tlc5940.h"

/*
BSD 2-Clause License

Copyright (c) 2017, Michael Dill
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
    Michael Dill - 20170224

    This sketch is designed to exercize the TLC5940 as a proof of concept.  In
    order to control LEDs, individually, we will have a series of on/off LEDs
    "chase" each other "across" the TLC5940.

 */

// Set the delay/refresh rate for the sketch
const byte wait = 50;

byte i = 15, j = 0;

void setup(){
    Tlc.init( 0 );
}

void loop(){
    Tlc.set( i, 1024 );
    Tlc.set( j, LOW );

    i++;
    if( i > 32 )
        i = 0;
        
    j++;
    if( j > 32 )
        j = 0;

    Tlc.update();
    delay( wait );
}

